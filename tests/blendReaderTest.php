<?php
/**
 * Copyright (C) 2013-2020 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 *
 **/

use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use PHPUnit\Framework\TestCase;

class blendReaderTest extends TestCase {
	/**
	 * @var \SheepItRenderFarm\BlendReader\BlendReaderWithLaunchingBlenderBinary
	 */
    private $reader;
    
    protected function setUp(): void {
        $log = new Logger('blend-reader');
        $log->pushHandler(new StreamHandler('/tmp/blend-reader.log', Logger::DEBUG));

        $this->reader = new \SheepItRenderFarm\BlendReader\BlendReaderWithLaunchingBlenderBinary($log, '/opt/blender4.0/rend.exe');
    }
    
    public function testFileDoesNotExist(): void {
        $blender_file = __DIR__.'/'.time().'.blend';
        
        while (file_exists($blender_file)) {
            $blender_file .= '.blend';
        }

        $this->reader->open($blender_file);
        $ret = $this->reader->getInfos();
        $this->assertFalse($ret);
    }
    
    public function testNotABlenderFile(): void {
        $blender_file = __DIR__.'/data/000000.png';
        
        $ret = $this->reader->open($blender_file);
        $this->assertTrue($ret);
        
        $infos = $this->reader->getInfos();
        
        $this->assertFalse($infos);
    }
    
    public function test302cycles1920x108040pcstart10end100(): void {
        $blender_file = __DIR__.'/data/302-cycles-1920x1080-40pc-start10-end100.blend';
        
        $ret = $this->reader->open($blender_file);
        $this->assertTrue($ret);
        
        $infos = $this->reader->getInfos();
        
        $this->assertTrue(is_array($infos));
        
        $this->assertTrue(array_key_exists('version', $infos));
        $this->assertTrue(array_key_exists('start_frame', $infos));
        $this->assertTrue(array_key_exists('end_frame', $infos));
        $this->assertTrue(array_key_exists('scene', $infos));
        $this->assertTrue(array_key_exists('output_file_extension', $infos));
        $this->assertTrue(array_key_exists('engine', $infos));
        $this->assertTrue(array_key_exists('resolution_percentage', $infos));
        $this->assertTrue(array_key_exists('resolution_x', $infos));
        $this->assertTrue(array_key_exists('resolution_y', $infos));
        $this->assertTrue(array_key_exists('missing_files', $infos));
        $this->assertTrue(array_key_exists('scripted_driver', $infos));
        $this->assertTrue(array_key_exists('cycles_samples', $infos));
        $this->assertTrue(array_key_exists('have_camera', $infos));
        
        $this->assertEquals('blender302', $infos['version']);
        $this->assertEquals(10, $infos['start_frame']);
        $this->assertEquals(100, $infos['end_frame']);
        $this->assertEquals('Scene', $infos['scene']);
        $this->assertEquals('CYCLES', $infos['engine']);
        $this->assertEquals((int)(1920 * 1080 * 0.40 * 0.40 * 10), $infos['cycles_samples']);
        $this->assertTrue($infos['have_camera']);
    }
    
    public function test302cycles200x100100pcstart1end250compressed(): void {
        $blender_file = __DIR__.'/data/302-cycles-200x100-100pcstart1-end250-compressed.blend';
        
        $ret = $this->reader->open($blender_file);
        $this->assertTrue($ret);
        
        $infos = $this->reader->getInfos();
        
        $this->assertTrue(is_array($infos));
        
        $this->assertTrue(array_key_exists('version', $infos));
        $this->assertTrue(array_key_exists('start_frame', $infos));
        $this->assertTrue(array_key_exists('end_frame', $infos));
        $this->assertTrue(array_key_exists('scene', $infos));
        $this->assertTrue(array_key_exists('output_file_extension', $infos));
        $this->assertTrue(array_key_exists('engine', $infos));
        $this->assertTrue(array_key_exists('resolution_percentage', $infos));
        $this->assertTrue(array_key_exists('resolution_x', $infos));
        $this->assertTrue(array_key_exists('resolution_y', $infos));
        $this->assertTrue(array_key_exists('missing_files', $infos));
        $this->assertTrue(array_key_exists('scripted_driver', $infos));
        $this->assertTrue(array_key_exists('cycles_samples', $infos));
        $this->assertTrue(array_key_exists('have_camera', $infos));
        
        $this->assertEquals('blender302', $infos['version']);
        $this->assertEquals(1, $infos['start_frame']);
        $this->assertEquals(250, $infos['end_frame']);
        $this->assertEquals('CYCLES', $infos['engine']);
        $this->assertEquals(200, $infos['resolution_x']);
        $this->assertEquals(100, $infos['resolution_y']);
        $this->assertEquals((int)(200 * 100 * 1 * 1 * 10), $infos['cycles_samples']);
        $this->assertTrue($infos['have_camera']);
    }
    
    public function test302cycles530x260100pcstart1end100(): void {
        $blender_file = __DIR__.'/data/302-cycles-530x260-100pc-start1-end100.blend';
        
        $ret = $this->reader->open($blender_file);
        $this->assertTrue($ret);
        
        $infos = $this->reader->getInfos();
        
        $this->assertTrue(is_array($infos));
        
        $this->assertTrue(array_key_exists('version', $infos));
        $this->assertTrue(array_key_exists('start_frame', $infos));
        $this->assertTrue(array_key_exists('end_frame', $infos));
        $this->assertTrue(array_key_exists('scene', $infos));
        $this->assertTrue(array_key_exists('output_file_extension', $infos));
        $this->assertTrue(array_key_exists('engine', $infos));
        $this->assertTrue(array_key_exists('resolution_percentage', $infos));
        $this->assertTrue(array_key_exists('resolution_x', $infos));
        $this->assertTrue(array_key_exists('resolution_y', $infos));
        $this->assertTrue(array_key_exists('missing_files', $infos));
        $this->assertTrue(array_key_exists('scripted_driver', $infos));
        $this->assertTrue(array_key_exists('cycles_samples', $infos));
        $this->assertTrue(array_key_exists('have_camera', $infos));
        
        $this->assertEquals('blender302', $infos['version']);
        $this->assertEquals(1, $infos['start_frame']);
        $this->assertEquals(100, $infos['end_frame']);
        $this->assertEquals('Scene', $infos['scene']);
        $this->assertEquals('CYCLES', $infos['engine']);
        $this->assertEquals((int)(530 * 260 * 1 * 1 * 150), $infos['cycles_samples']);
        $this->assertTrue($infos['have_camera']);
    }
    
    public function test302cycles530x26050pcstart1end100SquareSamples(): void {
        $blender_file = __DIR__.'/data/302-cycles-530x260-50pc-start1-end100-square-samples.blend';
        
        $ret = $this->reader->open($blender_file);
        $this->assertTrue($ret);
        
        $infos = $this->reader->getInfos();
        
        $this->assertTrue(is_array($infos));
        
        $this->assertTrue(array_key_exists('version', $infos));
        $this->assertTrue(array_key_exists('start_frame', $infos));
        $this->assertTrue(array_key_exists('end_frame', $infos));
        $this->assertTrue(array_key_exists('scene', $infos));
        $this->assertTrue(array_key_exists('output_file_extension', $infos));
        $this->assertTrue(array_key_exists('engine', $infos));
        $this->assertTrue(array_key_exists('resolution_percentage', $infos));
        $this->assertTrue(array_key_exists('resolution_x', $infos));
        $this->assertTrue(array_key_exists('resolution_y', $infos));
        $this->assertTrue(array_key_exists('missing_files', $infos));
        $this->assertTrue(array_key_exists('scripted_driver', $infos));
        $this->assertTrue(array_key_exists('cycles_samples', $infos));
        $this->assertTrue(array_key_exists('have_camera', $infos));
        
        $this->assertEquals('blender302', $infos['version']);
        $this->assertEquals(1, $infos['start_frame']);
        $this->assertEquals(100, $infos['end_frame']);
        $this->assertEquals('Scene', $infos['scene']);
        $this->assertEquals('CYCLES', $infos['engine']);
        $this->assertEquals((int)(530 * 260 * 0.50 * 0.50 * 1500 * 1500), $infos['cycles_samples']);
        $this->assertTrue($infos['have_camera']);
    }
    
    public function testPathWithQuote(): void {
        $blender_file = __DIR__."/data/path-with-'quote'-304-cycles-530x260-50pc-start1-end100-square-samples.blend";
        
        $ret = $this->reader->open($blender_file);
        $this->assertTrue($ret);
        
        $infos = $this->reader->getInfos();
        
        $this->assertTrue(is_array($infos));
        
        $this->assertTrue(array_key_exists('version', $infos));
        $this->assertTrue(array_key_exists('start_frame', $infos));
        $this->assertTrue(array_key_exists('end_frame', $infos));
        $this->assertTrue(array_key_exists('scene', $infos));
        $this->assertTrue(array_key_exists('output_file_extension', $infos));
        $this->assertTrue(array_key_exists('engine', $infos));
        $this->assertTrue(array_key_exists('resolution_percentage', $infos));
        $this->assertTrue(array_key_exists('resolution_x', $infos));
        $this->assertTrue(array_key_exists('resolution_y', $infos));
        $this->assertTrue(array_key_exists('missing_files', $infos));
        $this->assertTrue(array_key_exists('scripted_driver', $infos));
        $this->assertTrue(array_key_exists('cycles_samples', $infos));
        $this->assertTrue(array_key_exists('have_camera', $infos));
        
        $this->assertEquals('blender304', $infos['version']);
        $this->assertEquals(1, $infos['start_frame']);
        $this->assertEquals(100, $infos['end_frame']);
        $this->assertEquals('Scene', $infos['scene']);
        $this->assertEquals('CYCLES', $infos['engine']);
        $this->assertEquals((int)(530 * 260 * 0.50 * 0.50 * 1500 * 1500), $infos['cycles_samples']);
        $this->assertTrue($infos['have_camera']);
    }
    
    public function testPathWithSpace(): void {
        $blender_file = __DIR__."/data/path-with- -304-cycles-530x260-50pc-start1-end100-square-samples.blend";
        
        $ret = $this->reader->open($blender_file);
        $this->assertTrue($ret);
        
        $infos = $this->reader->getInfos();
        
        $this->assertTrue(is_array($infos));
        
        $this->assertTrue(array_key_exists('version', $infos));
        $this->assertTrue(array_key_exists('start_frame', $infos));
        $this->assertTrue(array_key_exists('end_frame', $infos));
        $this->assertTrue(array_key_exists('scene', $infos));
        $this->assertTrue(array_key_exists('output_file_extension', $infos));
        $this->assertTrue(array_key_exists('engine', $infos));
        $this->assertTrue(array_key_exists('resolution_percentage', $infos));
        $this->assertTrue(array_key_exists('resolution_x', $infos));
        $this->assertTrue(array_key_exists('resolution_y', $infos));
        $this->assertTrue(array_key_exists('missing_files', $infos));
        $this->assertTrue(array_key_exists('scripted_driver', $infos));
        $this->assertTrue(array_key_exists('cycles_samples', $infos));
        $this->assertTrue(array_key_exists('have_camera', $infos));
        
        $this->assertEquals('blender304', $infos['version']);
        $this->assertEquals(1, $infos['start_frame']);
        $this->assertEquals(100, $infos['end_frame']);
        $this->assertEquals('Scene', $infos['scene']);
        $this->assertEquals('CYCLES', $infos['engine']);
        $this->assertEquals((int)(530 * 260 * 0.50 * 0.50 * 1500 * 1500), $infos['cycles_samples']);
        $this->assertTrue($infos['have_camera']);
    }
    
    public function test302cycles24fps(): void {
        $blender_file = __DIR__.'/data/302-cycles-24fps.blend';
        
        $ret = $this->reader->open($blender_file);
        $this->assertTrue($ret);
        
        $infos = $this->reader->getInfos();
        
        $this->assertTrue(is_array($infos));
        
        $this->assertTrue(array_key_exists('version', $infos));
        $this->assertTrue(array_key_exists('framerate', $infos));
        $this->assertTrue(array_key_exists('have_camera', $infos));
        $this->assertTrue(array_key_exists('engine', $infos));
        
        $this->assertEquals('blender302', $infos['version']);
        $this->assertTrue($infos['have_camera']);
        $this->assertEquals('CYCLES', $infos['engine']);
        $this->assertEquals(24, $infos['framerate']);
    }
    
    public function testUnusedEnginesInCompositor(): void {
        $blender_file = __DIR__.'/data/304_renderEngineTest.blend';
        
        $ret = $this->reader->open($blender_file);
        $this->assertTrue($ret);
        
        $infos = $this->reader->getInfos();

        $this->assertTrue(is_array($infos));
        
        $this->assertTrue(array_key_exists('engine', $infos));
        $this->assertFalse(array_key_exists('cycles_samples', $infos));

        $this->assertEquals('BLENDER_EEVEE', $infos['engine']);
    }

    public function testMixedEnginesInCompositor(): void {
        $blender_file = __DIR__.'/data/304_renderEngineTestMixed.blend';
        
        $ret = $this->reader->open($blender_file);
        $this->assertTrue($ret);
        
        $infos = $this->reader->getInfos();

        $this->assertTrue(is_array($infos));
        
        $this->assertTrue(array_key_exists('engine', $infos));
        $this->assertFalse(array_key_exists('cycles_samples', $infos));

        $this->assertEquals('MIXED', $infos['engine']);
    }

    public function testBlender302CanUseTileCyclesWithCompositing(): void {
        $blender_file = __DIR__.'/data/302-cycles-compositing.blend';
        
        $ret = $this->reader->open($blender_file);
        $this->assertTrue($ret);
        
        $infos = $this->reader->getInfos();
        
        $this->assertTrue(is_array($infos));
        $this->assertTrue(array_key_exists('engine', $infos));
        $this->assertTrue(array_key_exists('can_use_tile', $infos));
        
        $this->assertEquals('CYCLES', $infos['engine']);
        $this->assertFalse($infos['can_use_tile']);
    }
    
    public function testBlender302CanUseTileCyclesWithoutCompositing(): void {
        $blender_file = __DIR__.'/data/302-cycles-no-compositing.blend';
        
        $ret = $this->reader->open($blender_file);
        $this->assertTrue($ret);
        
        $infos = $this->reader->getInfos();
        
        $this->assertTrue(is_array($infos));
        $this->assertTrue(array_key_exists('engine', $infos));
        $this->assertTrue(array_key_exists('can_use_tile', $infos));
        
        $this->assertEquals('CYCLES', $infos['engine']);
        $this->assertTrue($infos['can_use_tile']);
    }
    
    public function testBlender302CanUseTileCycleCompositingDisabled(): void {
        $blender_file = __DIR__.'/data/302-cycles-compositing-disabled.blend';
        
        $ret = $this->reader->open($blender_file);
        $this->assertTrue($ret);
        
        $infos = $this->reader->getInfos();
        
        $this->assertTrue(is_array($infos));
        $this->assertTrue(array_key_exists('engine', $infos));
        $this->assertTrue(array_key_exists('can_use_tile', $infos));
        
        $this->assertEquals('CYCLES', $infos['engine']);
        $this->assertTrue($infos['can_use_tile']);
    }
    
    public function testBlender302CanUseTileCycleCompositingEnableUseNodeDisabled(): void {
        $blender_file = __DIR__.'/data/302-cycles-compositing-disabled-use-node-disabled.blend';
        
        $ret = $this->reader->open($blender_file);
        $this->assertTrue($ret);
        
        $infos = $this->reader->getInfos();
        
        $this->assertTrue(is_array($infos));
        $this->assertTrue(array_key_exists('engine', $infos));
        $this->assertTrue(array_key_exists('can_use_tile', $infos));
        
        $this->assertEquals('CYCLES', $infos['engine']);
        $this->assertTrue($infos['can_use_tile']);
    }
    
    public function testBlender302CanUseTileCyclesWithoutDenoising(): void {
        $blender_file = __DIR__.'/data/302-cycles-no-denoising.blend';
        
        $ret = $this->reader->open($blender_file);
        $this->assertTrue($ret);
        
        $infos = $this->reader->getInfos();
        
        $this->assertTrue(is_array($infos));
        $this->assertTrue(array_key_exists('version', $infos));
        $this->assertTrue(array_key_exists('engine', $infos));
        $this->assertTrue(array_key_exists('can_use_tile', $infos));
        
        $this->assertEquals('blender302', $infos['version']);
        $this->assertEquals('CYCLES', $infos['engine']);
        $this->assertTrue($infos['can_use_tile']);
    }
    
    public function testBlender302CanUseTileCycleDenoising(): void {
        $blender_file = __DIR__.'/data/302-cycles-denoising.blend';
        
        $ret = $this->reader->open($blender_file);
        $this->assertTrue($ret);
        
        $infos = $this->reader->getInfos();
        
        $this->assertTrue(is_array($infos));
        $this->assertTrue(array_key_exists('version', $infos));
        $this->assertTrue(array_key_exists('engine', $infos));
        $this->assertTrue(array_key_exists('can_use_tile', $infos));
        
        $this->assertEquals('blender302', $infos['version']);
        $this->assertEquals('CYCLES', $infos['engine']);
        $this->assertFalse($infos['can_use_tile']);
    }
    
    public function test302Cycles(): void {
        $blender_file = __DIR__.'/data/302-cycles.blend';
        
        $this->assertTrue($this->reader->open($blender_file), 'Failed to open blend file '.$blender_file);
        
        $infos = $this->reader->getInfos();
        
        $this->assertTrue(is_array($infos));
        $this->assertTrue(array_key_exists('version', $infos));
        $this->assertTrue(array_key_exists('engine', $infos));
        
        $this->assertEquals('blender302', $infos['version']);
        $this->assertEquals('CYCLES', $infos['engine']);
    }
    
    public function test302Eevee(): void {
        $blender_file = __DIR__.'/data/302-eevee.blend';
        
        $this->assertTrue($this->reader->open($blender_file), 'Failed to open blend file '.$blender_file);
        
        $infos = $this->reader->getInfos();
        
        $this->assertTrue(is_array($infos));
        $this->assertTrue(array_key_exists('version', $infos));
        $this->assertTrue(array_key_exists('engine', $infos));
        
        $this->assertEquals('blender302', $infos['version']);
        $this->assertEquals('BLENDER_EEVEE', $infos['engine']);
    }
    
    public function test302Workbench(): void {
        $blender_file = __DIR__.'/data/302-workbench.blend';
        
        $this->assertTrue($this->reader->open($blender_file), 'Failed to open blend file '.$blender_file);
        
        $infos = $this->reader->getInfos();
        
        $this->assertTrue(is_array($infos));
        $this->assertTrue(array_key_exists('version', $infos));
        $this->assertTrue(array_key_exists('engine', $infos));
        
        $this->assertEquals('blender302', $infos['version']);
        $this->assertEquals('BLENDER_WORKBENCH', $infos['engine']);
    }
    
    public function testMissingFile(): void {
        $blender_file = __DIR__.'/data/304-missing-files.blend';
        
        $this->assertTrue($this->reader->open($blender_file), 'Failed to open blend file '.$blender_file);
        
        $infos = $this->reader->getInfos();
        
        $this->assertTrue(is_array($infos));
        $this->assertTrue(array_key_exists('missing_files', $infos));

        $this->assertTrue(is_array($infos['missing_files']));
        $this->assertEquals(1, count($infos['missing_files']));
        $this->assertEquals('//../../../br/tests/home/laurent/sheepit-contour.png', $infos['missing_files'][0]);
    }
    
    public function testDetectsActiveFileOutputNodes(): void {
        $blender_file = __DIR__.'/data/creating-images-outside-of-working-dir.blend';
        
        $this->assertTrue($this->reader->open($blender_file), 'Failed to open blend file '.$blender_file);
        
        $infos = $this->reader->getInfos();
        
        $this->assertTrue(is_array($infos));
        $this->assertTrue(array_key_exists('has_active_file_output_node', $infos));
        $this->assertTrue($infos['has_active_file_output_node']);
    }
    
    public function testDetectsActiveFileOutputNodesFalse(): void {
        $blender_file = dirname(__FILE__).'/data/302-cycles-denoising.blend';
        
        $this->assertTrue($this->reader->open($blender_file), 'Failed to open blend file '.$blender_file);
        
        $infos = $this->reader->getInfos();
        
        $this->assertTrue(is_array($infos));
        $this->assertTrue(array_key_exists('has_active_file_output_node', $infos));
        $this->assertFalse($infos['has_active_file_output_node']);
    }
    
    public function testDetectsActiveFileOutputNodesInActiveScenes(): void {
        $blender_file = __DIR__.'/data/304-output_file_node.blend';
        
        $this->assertTrue($this->reader->open($blender_file), 'Failed to open blend file '.$blender_file);
        
        $infos = $this->reader->getInfos();
        
        $this->assertTrue(is_array($infos));
        $this->assertTrue(array_key_exists('has_active_file_output_node', $infos));
        $this->assertTrue($infos['has_active_file_output_node']);
    }

    public function testDetectsActiveFileOutputNodesInActiveScenesMuted(): void {
        $blender_file = __DIR__.'/data/304-output_file_node_muted.blend';
        
        $this->assertTrue($this->reader->open($blender_file), 'Failed to open blend file '.$blender_file);
        
        $infos = $this->reader->getInfos();
        
        $this->assertTrue(is_array($infos));
        $this->assertTrue(array_key_exists('has_active_file_output_node', $infos));
        $this->assertFalse($infos['has_active_file_output_node']);
    }

    public function testDetectsGPencilObjects(): void {
        $blender_file = __DIR__.'/data/302-GPencil_detection.blend';
        
        $this->assertTrue($this->reader->open($blender_file), 'Failed to open blend file '.$blender_file);
        
        $infos = $this->reader->getInfos();
        
        $this->assertTrue(is_array($infos));
        $this->assertTrue(array_key_exists('has_GPencil_object', $infos));
        $this->assertTrue($infos['has_GPencil_object']);
    }

    public function testDetectsGPencilObjectsFalse(): void {
        $blender_file = __DIR__.'/data/304_default_scene.blend';
        
        $this->assertTrue($this->reader->open($blender_file), 'Failed to open blend file '.$blender_file);
        
        $infos = $this->reader->getInfos();
        
        $this->assertTrue(is_array($infos));
        $this->assertTrue(array_key_exists('has_GPencil_object', $infos));
        $this->assertFalse($infos['has_GPencil_object']);
    }

    public function testAviJpegOutput(): void {
        $blender_file = __DIR__.'/data/302-avi.jpg.blend';
        
        $this->assertTrue($this->reader->open($blender_file), 'Failed to open blend file '.$blender_file);
        
        $infos = $this->reader->getInfos();
        
        $this->assertTrue(is_array($infos));
        $this->assertTrue(array_key_exists('output_file_format', $infos));
        $this->assertTrue(array_key_exists('version', $infos));
        
        $this->assertEquals('blender302', $infos['version']);
        $this->assertEquals('AVI_JPEG', $infos['output_file_format']);
    }

    public function testAviRawOutput(): void {
        $blender_file = __DIR__.'/data/302-avi.raw.blend';
    
        $this->assertTrue($this->reader->open($blender_file), 'Failed to open blend file '.$blender_file);
    
        $infos = $this->reader->getInfos();
    
        $this->assertTrue(is_array($infos));
        $this->assertTrue(array_key_exists('output_file_format', $infos));
        $this->assertTrue(array_key_exists('version', $infos));
    
        $this->assertEquals('blender302', $infos['version']);
        $this->assertEquals('AVI_RAW', $infos['output_file_format']);
    }
    
    public function testFFmpegOutput(): void {
        $blender_file = __DIR__.'/data/302-ffmpeg.blend';
        
        $this->assertTrue($this->reader->open($blender_file), 'Failed to open blend file '.$blender_file);
        
        $infos = $this->reader->getInfos();
        
        $this->assertTrue(is_array($infos));
        $this->assertTrue(array_key_exists('output_file_format', $infos));
        $this->assertTrue(array_key_exists('version', $infos));
        
        $this->assertEquals('blender302', $infos['version']);
        $this->assertEquals('FFMPEG', $infos['output_file_format']);
    }
    
    public function testPNGOutput(): void {
        $blender_file = __DIR__.'/data/302-eevee.blend';
        
        $this->assertTrue($this->reader->open($blender_file), 'Failed to open blend file '.$blender_file);
        
        $infos = $this->reader->getInfos();
        
        $this->assertTrue(is_array($infos));
        $this->assertTrue(array_key_exists('output_file_format', $infos));
        $this->assertTrue(array_key_exists('version', $infos));
        
        $this->assertEquals('blender302', $infos['version']);
        $this->assertEquals('PNG', $infos['output_file_format']);
    }
    
    public function teststereo3dAnglyph(): void {
        $blender_file = __DIR__.'/data/302-stereo3d-anglyph.blend';
        
        $ret = $this->reader->open($blender_file);
        $this->assertTrue($ret);
        
        $infos = $this->reader->getInfos();
        
        $this->assertTrue(is_array($infos));
        
        $this->assertTrue(array_key_exists('resolution_x', $infos));
        $this->assertTrue(array_key_exists('resolution_y', $infos));
        
        $this->assertEquals(300, $infos['resolution_x']);
        $this->assertEquals(200, $infos['resolution_y']);
    }
    
    public function teststereo3dInterlace(): void {
        $blender_file = __DIR__.'/data/304-stereo3d-interlace.blend';
        
        $ret = $this->reader->open($blender_file);
        $this->assertTrue($ret);
        
        $infos = $this->reader->getInfos();
        
        $this->assertTrue(is_array($infos));
        
        $this->assertTrue(array_key_exists('resolution_x', $infos));
        $this->assertTrue(array_key_exists('resolution_y', $infos));
        
        $this->assertEquals(300, $infos['resolution_x']);
        $this->assertEquals(200, $infos['resolution_y']);
    }
    
    
    public function teststereo3dSideBySide(): void {
        $blender_file = __DIR__.'/data/304-stereo3d-side-by-side.blend';
        
        $ret = $this->reader->open($blender_file);
        $this->assertTrue($ret);
        
        $infos = $this->reader->getInfos();
        
        $this->assertTrue(is_array($infos));
        
        $this->assertTrue(array_key_exists('resolution_x', $infos));
        $this->assertTrue(array_key_exists('resolution_y', $infos));
        
        $this->assertEquals(300 * 2, $infos['resolution_x']);
        $this->assertEquals(200, $infos['resolution_y']);
    }
    
    
    public function teststereo3dTopBottom(): void {
        $blender_file = __DIR__.'/data/302-stereo3d-top-bottom.blend';
        
        $ret = $this->reader->open($blender_file);
        $this->assertTrue($ret);
        
        $infos = $this->reader->getInfos();
        
        $this->assertTrue(is_array($infos));
        
        $this->assertTrue(array_key_exists('resolution_x', $infos));
        $this->assertTrue(array_key_exists('resolution_y', $infos));
        
        $this->assertEquals(300, $infos['resolution_x']);
        $this->assertEquals(200 * 2, $infos['resolution_y']);
    }
    
    public function test304IsOptixEnabled(): void {
        $blender_file = __DIR__.'/data/304-OptiXDenoiserEnabled.blend';

        $ret = $this->reader->open($blender_file);
        $this->assertTrue($ret);

        $infos = $this->reader->getInfos();

        $this->assertTrue(is_array($infos));

        $this->assertTrue(array_key_exists('optix_is_active', $infos));
        $this->assertTrue($infos['optix_is_active']);
    }
    
    public function testGetVersion302(): void {
		$blender_file = __DIR__.'/data/302-cycles.blend';
	
		$ret = $this->reader->open($blender_file);
		$this->assertTrue($ret);
	
		$this->assertEquals('302', $this->reader->getVersion());
	}
	
	public function testGetVersion300(): void {
		$blender_file = __DIR__.'/data/300-cycles.blend';
		
		$ret = $this->reader->open($blender_file);
		$this->assertTrue($ret);
		
		$this->assertEquals('300', $this->reader->getVersion());
	}
	
	public function testGetVersionNotBlend(): void {
		$blender_file = __DIR__.'/data/000000.png';
		
		$ret = $this->reader->open($blender_file);
		$this->assertTrue($ret);
		
		$this->assertEquals('', $this->reader->getVersion());
	}
	
    public function testGetVersionTwice(): void {
        $blender_file = __DIR__.'/data/302-cycles.blend';
        
        $ret = $this->reader->open($blender_file);
        $this->assertTrue($ret);
        
        $this->assertEquals('302', $this->reader->getVersion());
        // get version should reset the read to begining of the file
        $this->assertEquals('302', $this->reader->getVersion());
    }
	
	public function testBlender300Info(): void {
		$blender_file = __DIR__.'/data/300-cycles.blend';
		
		$ret = $this->reader->open($blender_file);
		$this->assertTrue($ret);
		
		$infos = $this->reader->getInfos();
		
		$this->assertTrue(is_array($infos));
		$this->assertTrue(array_key_exists('version', $infos));
		$this->assertTrue(array_key_exists('engine', $infos));
		$this->assertTrue(array_key_exists('can_use_tile', $infos));
		
		$this->assertEquals('blender300', $infos['version']);
		$this->assertEquals('CYCLES', $infos['engine']);
		$this->assertFalse($infos['can_use_tile']); // It is False. The .blend uses Denoising
	}

    public function active_adaptive_sampling(): void {
        $blender_file = __DIR__.'/data/302-cycles-active_adaptive_sampling.blend';
        
        $this->assertTrue($this->reader->open($blender_file), 'Failed to open blend file '.$blender_file);
        
        $infos = $this->reader->getInfos();
        
        $this->assertTrue($infos['get_adaptive_sampling']);
    }

    public function no_active_adaptive_sampling(): void {
        $blender_file = __DIR__.'/data/302-cycles-compositing.blend';
        
        $this->assertTrue($this->reader->open($blender_file), 'Failed to open blend file '.$blender_file);
        
        $infos = $this->reader->getInfos();
        
        $this->assertFalse($infos['get_adaptive_sampling']);
    }

    public function testBlender320NotCompress(): void {
        $blender_file = __DIR__.'/data/320-not-compress.blend';

        $ret = $this->reader->open($blender_file);
        $this->assertTrue($ret);

        $infos = $this->reader->getInfos();

        $this->assertTrue(is_array($infos));
        $this->assertTrue(array_key_exists('version', $infos));
        $this->assertTrue(array_key_exists('engine', $infos));

        $this->assertEquals('blender302', $infos['version']);
        $this->assertEquals('CYCLES', $infos['engine']);
    }

    public function testBlender320Compress(): void {
        $blender_file = __DIR__.'/data/320-compress.blend';

        $ret = $this->reader->open($blender_file);

        $infos = $this->reader->getInfos();

        $this->assertTrue(is_array($infos));
        $this->assertTrue(array_key_exists('version', $infos));
        $this->assertTrue(array_key_exists('engine', $infos));

        $this->assertEquals('blender302', $infos['version']);
        $this->assertEquals('CYCLES', $infos['engine']);
    }

    public function testGetPasses(): void {
        $blender_file = __DIR__.'/data/302-passes.blend';

        $ret = $this->reader->open($blender_file);
        $this->assertTrue($ret);
        $infos = $this->reader->getInfos();

        $this->assertIsArray($infos);
        $this->assertArrayHasKey('enabled_passes', $infos);
        $this->assertCount(5 + 1, $infos['enabled_passes']); // + 1 for Alpha who is not on the UI but always enabled

        $this->assertArrayHasKey('must_be_disabled_passes', $infos);
        $this->assertCount(0, $infos['must_be_disabled_passes']);
    }

    public function testGetPassesMustDisabled(): void {
        $blender_file = __DIR__.'/data/302-passes-disabled-passes.blend';

        $ret = $this->reader->open($blender_file);
        $this->assertTrue($ret);
        $infos = $this->reader->getInfos();

        $this->assertArrayHasKey('enabled_passes', $infos);
        $this->assertArrayHasKey('version', $infos);

        $this->assertEquals('blender302', $infos['version']);
        $this->assertArrayHasKey('must_be_disabled_passes', $infos);
        $this->assertCount(4, $infos['must_be_disabled_passes']);
    }

    public function testGetPassesMustDisabled_304(): void {
        $blender_file = __DIR__.'/data/304-passes-disabled-passes.blend';

        $ret = $this->reader->open($blender_file);
        $this->assertTrue($ret);
        $infos = $this->reader->getInfos();

        $this->assertArrayHasKey('enabled_passes', $infos);
        $this->assertArrayHasKey('version', $infos);

        $this->assertEquals('blender304', $infos['version']);
        $this->assertArrayHasKey('must_be_disabled_passes', $infos);
        $this->assertCount(0, $infos['must_be_disabled_passes']);
    }

    public function testGetEXRCodec(): void {
        $blender_file = __DIR__.'/data/302-passes.blend';

        $ret = $this->reader->open($blender_file);
        $this->assertTrue($ret);
        $infos = $this->reader->getInfos();

        $this->assertArrayHasKey('exr_codec', $infos);
        $this->assertEquals("DWAB", $infos['exr_codec']);
    }

    public function testDetectSimulations(): void {
        $blender_file = __DIR__.'/data/302-detect-sim.blend';

        $ret = $this->reader->open($blender_file);
        $this->assertTrue($ret);
        $infos = $this->reader->getInfos();
        $this->assertArrayHasKey('simulations', $infos);
        $this->assertCount(4, $infos['simulations']);
    }

    public function testDetectSimulationsNone(): void {
        $blender_file = __DIR__.'/data/302-no-sim-detect.blend';

        $ret = $this->reader->open($blender_file);
        $this->assertTrue($ret);
        $infos = $this->reader->getInfos();

        $this->assertArrayHasKey('simulations', $infos);
        $this->assertCount(0, $infos['simulations']);
    }

    public function test302IsOptixEnabled(): void {
        $blender_file = __DIR__.'/data/302-OptiXDenoiserEnabled.blend';

        $ret = $this->reader->open($blender_file);
        $this->assertTrue($ret);

        $infos = $this->reader->getInfos();

        $this->assertIsArray($infos);

        $this->assertArrayHasKey('optix_is_active', $infos);
        $this->assertTrue($infos['optix_is_active']);
    }

    public function testBlender302Info(): void {
        $blender_file = __DIR__.'/data/302-cycles.blend';

        $ret = $this->reader->open($blender_file);
        $this->assertTrue($ret);

        $infos = $this->reader->getInfos();

        $this->assertIsArray($infos);
        $this->assertArrayHasKey('version', $infos);
        $this->assertArrayHasKey('engine', $infos);
        $this->assertArrayHasKey('can_use_tile', $infos);

        $this->assertEquals('blender302', $infos['version']);
        $this->assertEquals('CYCLES', $infos['engine']);
        $this->assertFalse($infos['can_use_tile']); // It is False. The .blend uses Denoising
    }

    public function testBlender330Info(): void {
        $blender_file = __DIR__.'/data/330-cycles.blend';

        $ret = $this->reader->open($blender_file);
        $this->assertTrue($ret);

        $infos = $this->reader->getInfos();

        $this->assertIsArray($infos);
        $this->assertArrayHasKey('version', $infos);
        $this->assertArrayHasKey('engine', $infos);

        $this->assertEquals('blender303', $infos['version']);
        $this->assertEquals('CYCLES', $infos['engine']);
    }

    public function testFindsActiveFileOutputNodeInNodeGroup(): void {
        $blender_file = __DIR__.'/data/305-output-file-node-in-nodegroup.blend';

        $ret = $this->reader->open($blender_file);
        $this->assertTrue($ret);

        $infos = $this->reader->getInfos();

        $this->assertIsArray($infos);
        $this->assertArrayHasKey('has_active_file_output_node', $infos);
        $this->assertTrue($infos['has_active_file_output_node']);
    }

    public function testFindActiveDenoiseNodeFalse(): void {
        $blender_file = __DIR__.'/data/305-muted-denoise-group.blend';

        $ret = $this->reader->open($blender_file);
        $this->assertTrue($ret);

        $infos = $this->reader->getInfos();

        $this->assertIsArray($infos);
        $this->assertArrayHasKey('has_denoising', $infos);
        $this->assertFalse($infos['has_denoising']);
    }

    public function testFindActiveDenoiseNodeTrue(): void {
        $blender_file = __DIR__.'/data/305-active-denoise-group.blend';

        $ret = $this->reader->open($blender_file);
        $this->assertTrue($ret);

        $infos = $this->reader->getInfos();

        $this->assertIsArray($infos);
        $this->assertArrayHasKey('has_denoising', $infos);
        $this->assertTrue($infos['has_denoising']);
    }

    public function testHasDenoisingEnabledTrue(): void {
        $blender_file = __DIR__.'/data/304-OptiXDenoiserEnabled.blend';

        $ret = $this->reader->open($blender_file);
        $this->assertTrue($ret);

        $infos = $this->reader->getInfos();

        $this->assertIsArray($infos);
        $this->assertArrayHasKey('has_denoising', $infos);
        $this->assertTrue($infos['has_denoising']);
    }

    public function test_pre_load_script(): void {
        #It is a very cpu heavy geo node project
        #If the script wouldn't work the test should take a long time 
        #and be very CPU intensive
        $blender_file = __DIR__.'/data/306-cpu-heavy-geo-nodes.blend';

        $ret = $this->reader->open($blender_file);
        $this->assertTrue($ret);

        $infos = $this->reader->getInfos();

        $this->assertIsArray($infos);
    }

    public function test_too_many_objects(): void {
        #It is a project with 53000 obcjets
        #If we don't optimize the blend reader, the upload process will time out
        $blender_file = __DIR__.'/data/400_53K_Too_many_objects.blend';

        $ret = $this->reader->open($blender_file);
        $this->assertTrue($ret);

        $infos = $this->reader->getInfos();

        $this->assertIsArray($infos);
    }

	public function test400cycles(): void {
		$blender_file = __DIR__.'/data/400-cycles.blend';

		$ret = $this->reader->open($blender_file);
		$this->assertTrue($ret);

		$infos = $this->reader->getInfos();

		$this->assertTrue(is_array($infos));

		$this->assertArrayHasKey('version', $infos);
		$this->assertArrayHasKey('engine', $infos);

		$this->assertEquals('blender400', $infos['version']);
		$this->assertEquals('CYCLES', $infos['engine']);
	}

	public function test400eevee(): void {
		$blender_file = __DIR__.'/data/400-eevee.blend';

		$ret = $this->reader->open($blender_file);
		$this->assertTrue($ret);

		$infos = $this->reader->getInfos();

		$this->assertTrue(is_array($infos));

		$this->assertArrayHasKey('version', $infos);
		$this->assertArrayHasKey('engine', $infos);

		$this->assertEquals('blender400', $infos['version']);
		$this->assertEquals('BLENDER_EEVEE', $infos['engine']);

		$this->assertArrayHasKey('supported_Color_Management', $infos);
		$this->assertTrue($infos['supported_Color_Management']);
	}

    public function testColorManagement(): void {
		$blender_file = __DIR__.'/data/306-ACES.blend';

		$ret = $this->reader->open($blender_file);
		$this->assertTrue($ret);

		$infos = $this->reader->getInfos();

		$this->assertTrue(is_array($infos));

		$this->assertArrayHasKey('version', $infos);
		$this->assertArrayHasKey('engine', $infos);

		$this->assertEquals('blender306', $infos['version']);
		$this->assertEquals('CYCLES', $infos['engine']);

		$this->assertArrayHasKey('supported_Color_Management', $infos);
		$this->assertFalse($infos['supported_Color_Management']);
	}
}
